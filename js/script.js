paragraphs = document.querySelectorAll('p');
paragraphs.forEach(paragraph => {
    paragraph.style.backgroundColor = '#ff0000';
});

optionsList = document.getElementById('optionsList');
console.log(optionsList);

parentElement = optionsList.parentElement;
console.log(parentElement);

if (optionsList.childNodes.length > 0) {
    optionsList.childNodes.forEach(node => {
        console.log('Node name: ' + node.nodeName + ', node type: ' + node.nodeType);
    });
}

var element = document.getElementById('testParagraph');
element.textContent = 'This is a paragraph';

mainHeader = document.querySelector('.main-header');
underElements = mainHeader.querySelectorAll('*');

underElements.forEach(element => {
    element.classList.add('nav-item');
    console.log(element);
});

sectionTitles = document.querySelectorAll('.section-title');
sectionTitles.forEach(title => {
    title.classList.remove('section-title');
});

